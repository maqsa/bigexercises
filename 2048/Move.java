package com.javarush.task.task35.task3513;

/**
 * Created by maqsa on 13.06.2017.
 */
@FunctionalInterface
public interface Move {
    void move();
}
