package com.javarush.test.level28.lesson15.big01.model;

import com.javarush.test.level28.lesson15.big01.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by maqsa on 29.12.2015.
 */
public class MoikrugStrategy implements Strategy
{
    private static final String URL_FORMAT = "http://javarush.ru/testdata/big28data2.html";

    protected Document getDocument(String searchString, int page) throws IOException
    {

        String url = String.format(URL_FORMAT, searchString, page);

        String agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0";
        String ref = "http://google.com.ua";
        Document doc = Jsoup.connect(url).userAgent(agent).referrer(ref).get();

        return doc;
    }

    public List<Vacancy> getVacancies(String searchString)
    {
        List<Vacancy> vacancies = new ArrayList<>();
        try
        {
            Document document;
            int pageCounter = 1;

            while(true)
            {
                document = getDocument(searchString, pageCounter++);
                if(document == null) break;

                Elements elements = document.getElementsByClass("job");
                if(elements.size() == 0) break;

                for(Element element : elements)
                {
                    Vacancy vacancy = new Vacancy();

                    vacancy.setUrl( element.getElementsByClass("title").first().child(0).attr("abs:href") );
                    vacancy.setTitle( element.getElementsByAttributeValue("class", "title").text() );
                    vacancy.setCity( element.getElementsByAttributeValue("class", "location").text() );
                    vacancy.setSalary( element.getElementsByAttributeValue("class", "salary").text() );
                    vacancy.setSiteName("https://moikrug.ru/");
                    vacancy.setCompanyName( element.getElementsByAttributeValue("class", "company_name").first().child(0).text() );

                    vacancies.add(vacancy);
                }
            }
        }
        catch (Exception e)
        {

        }

        return vacancies;
    }
}
