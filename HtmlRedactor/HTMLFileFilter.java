package com.javarush.test.level32.lesson15.big01;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by maqsa on 24.01.2016.
 */
public class HTMLFileFilter extends FileFilter
{
    @Override
    public boolean accept(File file) {
        String filename = file.getName().toLowerCase();
        return (file.isDirectory() || (filename.endsWith(".html") || filename.endsWith(".htm")));
    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }
}
