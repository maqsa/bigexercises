package com.javarush.task.task35.task3513;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by maqsa on 10.06.2017.
 */
public class Controller extends KeyAdapter {
    private Model   model;
    private View    view;
    private final static int WINNING_TILE = 2048;


    public View getView() {
        return view;
    }

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public Controller(Model model) {
        this.model = model;
        this.view = new View(this);
    }

    public Tile[][] getGameTiles() {
        return model.getGameTiles();
    }
    public int getScore() {
        return model.score;
    }

    public void resetGame(){
        model.score     =   0;
        view.isGameWon  =   false;
        view.isGameLost =   false;
        model.resetGameTiles();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (KeyEvent.VK_ESCAPE == e.getKeyCode()) resetGame();
        if (KeyEvent.VK_Z == e.getKeyCode()) model.rollback();
        if (KeyEvent.VK_R == e.getKeyCode()) model.randomMove();
        if (KeyEvent.VK_A == e.getKeyCode()) model.autoMove();
        if (!model.canMove()) view.isGameLost = true;
        if (view.isGameWon  ==  false && view.isGameLost ==  false){
            if (KeyEvent.VK_LEFT    ==  e.getKeyCode())     model.left();
            if (KeyEvent.VK_RIGHT   ==  e.getKeyCode())     model.right();
            if (KeyEvent.VK_UP      ==  e.getKeyCode())     model.up();
            if (KeyEvent.VK_DOWN    ==  e.getKeyCode())     model.down();
        }

        if (model.maxTile == WINNING_TILE) view.isGameWon = true;
        view.repaint();
    }
}