package com.javarush.test.level28.lesson15.big01;

import com.javarush.test.level28.lesson15.big01.model.*;
import com.javarush.test.level28.lesson15.big01.view.HtmlView;
import com.javarush.test.level28.lesson15.big01.vo.Vacancy;

import java.io.IOException;
import java.util.*;

/**
 * Created by maqsa on 24.12.2015.
 */
public class Aggregator
{
    public static void main(String[] args) throws IOException
    {
        Provider hhProvider = new Provider(new HHStrategy());
        Provider mkProvider = new Provider(new MoikrugStrategy());

        HtmlView view = new HtmlView();
        Model model = new Model(view, new Provider[] {hhProvider, mkProvider});
        Controller controller = new Controller(model);
        view.setController(controller);

        view.userCitySelectEmulationMethod();
    }
}
